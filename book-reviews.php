<?php
/*
Plugin Name: Book Reviews
Plugin URI:
Description: Declares a plugin that will create a custom post type displaying book reviews.
Author: Mrinal Haque
Author URI: https://www.facebook.com/Md.MrinalHaque
Version: 0.1
License: GPLv3

Book Reviews is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
 
Book Reviews is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with Book Reviews. If not, see http://www.gnu.org/licenses/gpl-3.0.html.
*/

// Create Book Reviews Menu on Dashboard

add_action( 'init', 'create_book_review' );

function create_book_review() {
	$labels = array(
		'name'			=> 'Book Reviews',
		'singular_name'	=> 'Book Review',
		'add_new'		=> 'Add New Book',
		'add_new_item' 	=> 'Add New Book',
		);
	$args = array(
		'labels' 		=> $labels,
		'public'		=> true,
		//'menu_position'	=> 5,
		'supports'		=> array( 'title', 'thumbnail'),
		'has_archive'	=> true,
		'menu_icon'		=> 'dashicons-book-alt',
		);
	register_post_type( 'book', $args );
}

function wpb_change_title_text( $title ){
     $screen = get_current_screen();
 
     if  ( 'book' == $screen->post_type ) {
          $title = 'Book Name Here';
     }
 
     return $title;
}
 
add_filter( 'enter_title_here', 'wpb_change_title_text' );


function myplugin_activate() {
    // register taxonomies/post types here
    flush_rewrite_rules();
}

//register_activation_hook( __FILE__, 'myplugin_activate' );

function myplugin_deactivate() {
    flush_rewrite_rules();
}
//register_deactivation_hook( __FILE__, 'myplugin_deactivate' );

/*
This plugin use CMB2 toolkit
*/

// cmb2/init.php including

require_once  plugin_dir_path( __FILE__ ) . 'libs/cmb2/init.php';


add_action( 'cmb2_init', 'book_post_metaboxes' );

function book_post_metaboxes() {

	$prefix = '_book_';

	// Initiat the metabox
	$cmb = new_cmb2_box( array(
			'id'			=> 'book_metabox',
			'title'			=> 'Book Information',
			'object_types'	=> array('book'),
			'context'       => 'normal',
			'show_names'	=> true,
	) );

	$cmb->add_field( array(
			'name'			=> __( 'Description', 'cmb2' ),
			'desc'			=> __( 'Description about this book', 'cmb2' ),
			'id'			=> $prefix . 'editor',
			'type'			=> 'wysiwyg',
	) );

	$cmb->add_field( array(
			'name'			=> __( 'Publisher', 'cmb2' ),
			'desc'			=> __( 'Publisher Name (Requered)', 'cmb2' ),
			'id'			=> $prefix . 'text1',
			'type'			=> 'text',
	) );

	$cmb->add_field( array(
			'name'			=> __( 'Author', 'cmb2' ),
			'desc'			=> __( 'Author Name (Requered)', 'cmb2' ),
			'id'			=> $prefix . 'text2',
			'type'			=> 'text',
	) );

	$cmb->add_field( array(
			'name'			=> __( 'ISBN', 'cmb2' ),
			'desc'			=> __( 'ISBN (Requered)', 'cmb2' ),
			'id'			=> $prefix . 'text3',
			'type'			=> 'text',
	) );

	$cmb->add_field( array(
			'name'			=> __( 'Language', 'cmb2' ),
			'desc'			=> __( 'Language', 'cmb2' ),
			'id'			=> $prefix . 'text4',
			'type'			=> 'text',
	) );

	$cmb->add_field( array(
			'name'			=> __( 'Pages', 'cmb2' ),
			'desc'			=> __( 'Total Page', 'cmb2' ),
			'id'			=> $prefix . 'text5',
			'type'			=> 'text',
	) );

	$cmb->add_field( array(
			'name'			=> __( 'Published', 'cmb2' ),
			'desc'			=> __( 'Published Date', 'cmb2' ),
			'id'			=> $prefix . 'text_date_timestamp',
			'show_option_none' => true,
			'type'			=> 'text_date_timestamp',
	) );

}

add_filter( 'manage_book_posts_columns', 'columns_book' );

function columns_book( $columns ) {
	unset( $columns['date'] );
	unset( $columns['author'] );
	unset( $columns['title'] );
	return array_merge( $columns, 
              array( 'title'		=> __('Books'),
              		'publisher'	=> __('Publisher'),
              		'isbn' 		=> __('ISBN'),
              		'writter'	=> __('Author'),

              	) );
}

add_action( 'manage_book_posts_custom_column', 'book_post_columns_data' );

function book_post_columns_data( $columns ) {
    if( 'writter' == $columns ) {
        $author = esc_html( get_post_meta( get_the_ID(), '_book_text2', true ) );
        echo '<a href="' . $author . '">' . $author. '</a>';
    } elseif( 'publisher' == $columns ) {
        $publisher = esc_html( get_post_meta( get_the_id(), '_book_text1', true ) );
        echo $publisher;
    } elseif( 'isbn' == $columns ) {
    	$isbn = esc_html( get_post_meta( get_the_id(), '_book_text3', true ) );
    	echo $isbn;
    }
}

add_filter( "manage_edit-book_sortable_columns", "sortable_columns" );

function sortable_columns() {
  return array(
    'publisher'			=> __('Publisher'),
    'isbn'				=> __('ISBN'),
    'writter'			=> __('Author'),
    'title'				=> __('Title')
  );
}

add_filter( 'template_include', 'include_book_template_function', 1 );

function include_book_template_function( $template_path ) {
    if ( get_post_type() == 'book' ) {
        if ( is_single() ) {
            // checks if the file exists in the theme first,
            // otherwise serve the file from the plugin
            if ( $theme_file = locate_template( array ( 'single-book_reviews.php' ) ) ) {
                $template_path = $theme_file;
            } else {
                $template_path = plugin_dir_path( __FILE__ ) . '/single-book_reviews.php';
            }
        }
        elseif ( is_archive() ) {
            if ( $theme_file = locate_template( array ( 'archive-book_reviews.php' ) ) ) {
                $template_path = $theme_file;
            } else { $template_path = plugin_dir_path( __FILE__ ) . '/archive-book_reviews.php';
 
            }
        }
    }
    return $template_path;
}

add_action( 'init', 'create_book_taxonomies', 0 );

function create_book_taxonomies() {
    register_taxonomy(
        'book_categories',
        'book',
        array(
            'labels' => array(
                'name' => 'Book Categories',
                'add_new_item' => 'Add New Book Category',
                'new_item_name' => "New Book Category"
            ),
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true
        )
    );
}