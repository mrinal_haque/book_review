<?php
/*
Template Name: Single Book
*/
get_header();
?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
	<?php
    $mypost = array( 'post_type' => 'book', );
    $loop = new WP_Query( $mypost );
    if( have_posts() ) :
    ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
		<div style="float: right; margin: 10px">
                    <?php the_post_thumbnail( array( 150, 200 ) ); ?>
                </div>
 
                <!-- Display Title and Author Name -->
                <strong>Book: <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> </strong><br />
                <strong>Author: </strong>
                <?php echo esc_html( get_post_meta( get_the_ID(), '_book_text2', true ) ); ?>
                <br />
                <strong>Publisher: </strong>
                <?php echo esc_html( get_post_meta( get_the_ID(), '_book_text1', true ) ); ?>
                <br />
                <strong>ISBN: </strong>
                <?php echo esc_html( get_post_meta( get_the_ID(), '_book_text3', true ) ); ?>
                <br />
                <strong>Pages: </strong>
                <?php echo esc_html( get_post_meta( get_the_ID(), '_book_text5', true ) ); ?>
                <br />
                <strong>Language: </strong>
                <?php echo esc_html( get_post_meta( get_the_ID(), '_book_text4', true ) ); ?>
                <br />
                <strong>Category: </strong>
                <?php  
                the_terms( $post->ID, 'book_categories' ,  ' ' );
                ?>
                <br/>
                <div ><strong>Description:</strong> <?php echo esc_html( get_post_meta( get_the_ID(), '_book_editor', true ) ); ?></div>

	</article>
	<?php endif; ?>
	<?php
	the_post_navigation( array(
				'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'twentyfifteen' ) . '</span> ' .
					'<span class="screen-reader-text">' . __( 'Next post:', 'twentyfifteen' ) . '</span> ' .
					'<span class="post-title">%title</span>',
				'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'twentyfifteen' ) . '</span> ' .
					'<span class="screen-reader-text">' . __( 'Previous post:', 'twentyfifteen' ) . '</span> ' .
					'<span class="post-title">%title</span>',
			) );
	?>
	</main>
</div>
<?php
get_footer();
?>