<?php
/*
Template Name: Book Archive
*/

get_header();
?>

<div id="primary" class="content-area">
    <main id="content" class="site-main" role="main">
    <?php
    $mypost = array( 'post_type' => 'book', );
    $loop = new WP_Query( $mypost );
    ?>
    <?php while ( $loop->have_posts() ) : $loop->the_post();?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <header class="entry-header">
            <!-- Comment Remove-->
 
                <!-- Display featured image in right-aligned floating div -->
                <div style="float: right; margin: 10px">
                    <?php the_post_thumbnail( array( 100, 100 ) ); ?>
                </div>
 
                <!-- Display Title and Author Name -->
                <strong>Book: <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> </strong><br />
                <strong>Author: </strong>
                <?php echo esc_html( get_post_meta( get_the_ID(), '_book_text2', true ) ); ?>
                <br />
                <strong>Publisher: </strong>
                <?php echo esc_html( get_post_meta( get_the_ID(), '_book_text1', true ) ); ?>
                <br />
                <strong>Category: </strong>
                <?php  
                the_terms( $post->ID, 'book_categories' ,  ' ' );
                    ?>
                <br/>
 
                
            </header>
             
        </article>
 
    <?php endwhile; ?>
    </main>
</div>
<?php wp_reset_query(); ?>

<?php
get_footer();
?>